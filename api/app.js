const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3030;

app.use(bodyParser.json());
app.use('/images', express.static('images'));

const auth = require('./controllers/auth.js');
const users = require('./controllers/users.js');
const bikes = require('./controllers/bikes.js');
const bookings = require('./controllers/bookings.js');

app.use('/auth', auth);
app.use('/users', users);
app.use('/bikes', bikes);
app.use('/bookings', bookings);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
