const express = require('express');
const moment = require('moment');
const router = express.Router();
const db = require('../modules/db');
const fs = require('fs');
const formidable = require('formidable');

router.get('/', function (req, res) {
	const query = req.query;
	let where = "";

	for ( let i in query ) {
		if ( !query[i] ) {
			continue;
		}

		let operator = "=";
		let value = query[i];

		if ( i.indexOf('min_') !== -1 ) {
			operator = '>=';
		} else if ( i.indexOf('max_') !== -1 ) {
			operator = '<=';
		}

		if ( i === 'model' || i === 'color' ) {
			value = "'" + value + "'";
		}

		where = where + " AND " + i.replace('min_', '').replace('max_', '') + " " + operator + " " + value;
	}

	db.bikesModel.find("SELECT * FROM bikes WHERE TRUE" + where, function(err, entries) {
	    const bikes = [];

	    for ( let i in entries ) {
	    	bikes.push(entries[i].getAll());
	    }

	    res.json(bikes);
	});
});

router.get('/:id', function (req, res) {
	const id = req.params.id;
  	
  	db.bikesModel.findOne({ id: id }, function(err, entry) {
  		if (!entry)
  			res.status(404);

    	res.json(entry ? entry.getAll() : {});
	});
});

router.post('/', function (req, res) {
	const now = moment().format('YYYY-MM-DD HH:mm:ss');
	const uniqid = new Date().getTime();
	const model = db.bikesModel.createModel( Object.assign( req.body, { uniqid: uniqid, created_at: now, updated_at: now } ));

	model.create(function(err) {
		if (err) {
			res.status(500);
			res.json({ error: err });
		} else {
			res.json(model);
		}
	});
});

router.post('/:id/photo', function (req, res) {
	const id = req.params.id;
	const form = new formidable.IncomingForm();
 
    form.parse(req, function(err, fields, files) {      	
      	fs.rename(files['file'].path, './images/' + id + '.' + /(?:\.([^.]+))?$/.exec(files['file'].name)[1], function (err) {
        	if (err) 
        		throw err;
        	
        	res.json({ id: id });
      	});
    });
});

router.put('/:id', function (req, res) {
	const id = req.params.id;
	const now = moment().format('YYYY-MM-DD HH:mm:ss');

	db.bikesModel.findOne({ id: id }, function(err, entry) {
  		if (!entry) {
  			res.status(404);
  			res.json({});
  		} else {
  			for ( let i in req.body ) {
  				entry.set(i, req.body[i]);
  			}

  			entry.set('updated_at', now);

  			entry.update(function(err, model) {
				if (err) {
					res.status(500);
					res.json({ error: err });
				} else {
					res.json(model);
				}
			});
  		}
	});
});

router.delete('/:id', function (req, res) {
	const id = req.params.id;

	db.bikesModel.findOne({ id: id }, function(err, entry) {
  		if (!entry) {
  			res.status(404);
  			res.json({});
  		} else {
  			entry.remove(function(err) {
				res.json({});
			});
  		}
	});
});

module.exports = router;
