const express = require('express');
const moment = require('moment');
const router = express.Router();
const db = require('../modules/db');

router.get('/', function (req, res) {
	const query = req.query;
	let where = "";

	for ( let i in query ) {
		if ( !query[i] ) {
			continue;
		}

		let operator = "=";
		let value = query[i];

		where = where + " AND " + i + " " + operator + " " + value;
	}

	db.bikesModel.find(
			"SELECT bookings.*, bikes.name as bike_name, users.name as user_name " +
			"FROM bookings " +
			"LEFT JOIN bikes ON bookings.bike_id = bikes.id " +
			"LEFT JOIN users ON bookings.user_id = users.id " +
			"WHERE TRUE" + where, 
	function(err, entries) {
	    const bikes = [];

	    for ( let i in entries ) {
	    	bikes.push(entries[i].getAll());
	    }

	    res.json(bikes);
	});
});

router.get('/:id', function (req, res) {
	const id = req.params.id;
  	
  	db.bookingsModel.findOne({ id: id }, function(err, entry) {
  		if (!entry)
  			res.status(404);

    	res.json(entry ? entry.getAll() : {});
	});
});

router.post('/', function (req, res) {
	const now = moment().format('YYYY-MM-DD HH:mm:ss');
	const model = db.bookingsModel.createModel( Object.assign( req.body, { created_at: now, updated_at: now } ));
	
	model.create(function(err) {
		if (err) {
			res.status(500);
			res.json({ error: err });
		} else {
			res.json(model);
		}
	});
});

router.put('/:id', function (req, res) {
	const id = req.params.id;
	const now = moment().format('YYYY-MM-DD HH:mm:ss');

	db.bookingsModel.findOne({ id: id }, function(err, entry) {
  		if (!entry) {
  			res.status(404);
  			res.json({});
  		} else {
  			for ( let i in req.body ) {
  				entry.set(i, req.body[i]);
  			}

  			entry.set('updated_at', now);

  			entry.update(function(err, model) {
				if (err) {
					res.status(500);
					res.json({ error: err });
				} else {
					res.json(model);
				}
			});
  		}
	});
});

router.delete('/:id', function (req, res) {
	const id = req.params.id;

	db.bookingsModel.findOne({ id: id }, function(err, entry) {
  		if (!entry) {
  			res.status(404);
  			res.json({});
  		} else {
  			entry.remove(function(err) {
				res.json({});
			});
  		}
	});
});

module.exports = router;
