const express = require('express');
const moment = require('moment');
const bcrypt = require('bcrypt');
const router = express.Router();
const db = require('../modules/db');

router.get('/', function (req, res) {
	db.usersModel.findAll(function(err, entries) {
	    const users = [];

	    for ( let i in entries ) {
	    	users.push(entries[i].getAll());
	    }

	    res.json(users);
	});
});

router.get('/:id', function (req, res) {
	const id = req.params.id;
  	
  	db.usersModel.findOne({ id: id }, function(err, entry) {
  		if (!entry)
  			res.status(404);

    	res.json(entry ? entry.getAll() : {});
	});
});

router.post('/', function (req, res) {
	const now = moment().format('YYYY-MM-DD HH:mm:ss');
	const model = db.usersModel.createModel( 
		Object.assign(
			req.body, 
			{ 
				created_at: now, 
				updated_at: now, 
				password: bcrypt.hashSync(req.body.password, 10)
			} 
		)
	);
	
	model.create(function(err) {
		if (err) {
			res.status(500);
			res.json({ error: err });
		} else {
			res.json(model);
		}
	});
});

router.put('/:id', function (req, res) {
	const id = req.params.id;
	const now = moment().format('YYYY-MM-DD HH:mm:ss');

	db.usersModel.findOne({ id: id }, function(err, entry) {
  		if (!entry) {
  			res.status(404);
  			res.json({});
  		} else {
  			for ( let i in req.body ) {
  				entry.set(i, req.body[i]);
  			}

  			entry.set('updated_at', now);
  			entry.set('password', bcrypt.hashSync(req.body.password, 10));

  			entry.update(function(err, model) {
				if (err) {
					res.status(500);
					res.json({ error: err });
				} else {
					res.json(model);
				}
			});
  		}
	});
});

router.delete('/:id', function (req, res) {
	const id = req.params.id;

	db.usersModel.findOne({ id: id }, function(err, entry) {
  		if (!entry) {
  			res.status(404);
  			res.json({});
  		} else {
  			entry.remove(function(err) {
				res.json({});
			});
  		}
	});
});

module.exports = router;
