const express = require('express');
const moment = require('moment');
const bcrypt = require('bcrypt');
const router = express.Router();
const db = require('../modules/db');

router.post('/login', function (req, res) {
	db.usersModel.findOne({ email: req.body.email }, function(err, entry) {
  		if (!entry)
  			res.status(404);

  		if (!bcrypt.compareSync(req.body.password, entry.get('password')))
  			res.status(404).end();
  		else
    		res.json(entry);
	});
});

router.post('/register', function (req, res) {
	const now = moment().format('YYYY-MM-DD HH:mm:ss');
	const model = db.usersModel.createModel( 
		Object.assign( 
			req.body, 
			{ 
				created_at: now, 
				updated_at: now, 
				password: bcrypt.hashSync(req.body.password, 10)
			} 
		)
	);
	
	model.create(function(err) {
		if (err) {
			res.status(500);
			res.json({ error: err });
		} else {
			res.json(model);
		}
	});
});

module.exports = router;

