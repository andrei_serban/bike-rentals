const mysql = require('mysql');
const db = require('light-orm');

db.driver = mysql.createConnection(require('../connection.json'));
db.driver.connect();

const users = new db.Collection('users');
const bikes = new db.Collection('bikes');
const bookings = new db.Collection('bookings');

module.exports = {
	db: db,
	usersModel: users,
	bikesModel: bikes,
	bookingsModel: bookings
};
