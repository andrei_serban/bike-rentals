export const API_URL = 'http://localhost:3030';

export const USER_ID_KEY = '@BikeRentals:UserId_001';

export const USER_ROLES = ['User', 'Manager'];

export const BIKE_MODELS = ['Mountain Bike', 'City Bike', 'Speed Bike'];
export const BIKE_COLORS = ['Black', 'White', 'Red', 'Blue', 'Green', 'Yellow'];

export const MIN_WEIGHT = 1;
export const MAX_WEIGHT = 200;
export const MIN_RATE = 1;
export const MAX_RATE = 100;
