import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default class CustomItem extends React.Component {
  render() {
  	const { children, label } = this.props;

    return (
      <View style={ styles.wrapper }>
      	<Text style={ styles.label }>{ label }</Text>
      	<View style={ styles.content }>
  		  { children }
  		</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    paddingTop: 25,
    paddingLeft: 15,
    paddingRight: 15
  },
  label: {
	fontSize: 17,
	color: '#555'
  },
  content: {
	paddingTop: 15
  }
});

