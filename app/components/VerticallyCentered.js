import React from 'react';
import { View } from 'react-native';

export default class VerticallyCentered extends React.Component {
  render() {
  	const { children } = this.props;

    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
      	{ children }
      </View>
    );
  }
}
