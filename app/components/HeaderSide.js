import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Icon } from 'expo';

import Colors from '../constants/Colors';

export default class HeaderSide extends React.Component {
  render() {
  	const { name, onPress } = this.props;
    
    return (
		<TouchableOpacity onPress={ onPress } style={{ paddingLeft: 15, paddingRight: 15 }}>
	      	<Icon.Ionicons
		        name={ name }
		        size={ 32 }
		        color={ Colors.tabIconSelected }
	      	/>
      	</TouchableOpacity>
    );
  }
}
