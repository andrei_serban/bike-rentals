import React from 'react';
import { View } from 'react-native';

export default class PaddedArea extends React.Component {
  render() {
  	const { children, top, xl } = this.props;
  	const padding = xl ? 20 : 15;

    return (
      <View style={ top ? { paddingTop: padding } : { padding: padding } }>
      	{ children }
      </View>
    );
  }
}
