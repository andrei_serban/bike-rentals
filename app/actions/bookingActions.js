import * as types from './actionTypes';
import axios from 'axios';
import { API_URL } from '../constants/Globals';

export function receiveBooking(data) {
	return { type: types.RECEIVE_BOOKING, booking: data };
}

export function createBooking(data) {
	return dispatch => {
    	return axios.post( API_URL + '/bookings', data ).then(response => dispatch(receiveBooking(response.data)));
  	};
}

export function updateBooking(id, data) {
  return dispatch => {
      return axios.put( API_URL + '/bookings/' + id, data ).then(response => dispatch(receiveBooking(response.data)));
    };
}

export function deleteBooking(id) {
	return dispatch => {
    	return axios.delete( API_URL + '/bookings/' + id ).then(response => dispatch(receiveBooking(response.data)));
  	};
}
