import * as types from './actionTypes';
import axios from 'axios';
import { API_URL } from '../constants/Globals';

export function receiveBookings(data) {
	return { type: types.RECEIVE_BOOKINGS, bookings: data };
}

export function fetchBookings(params) {
	let query = '';

	for ( let i in params ) {
		query = ( query === '' ? '?' : '&' ) + i + '=' + params[i];
	}

	console.log('query', API_URL + '/bookings' + query);
	
	return dispatch => {
    	return axios.get( API_URL + '/bookings' + query ).then(response => dispatch(receiveBookings(response.data)));
  	};
}
