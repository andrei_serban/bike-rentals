import * as types from './actionTypes';
import axios from 'axios';
import { API_URL } from '../constants/Globals';

export function receiveUser(data) {
  	return { type: types.RECEIVE_USER, user: data };
}

export function fetchUser(id) {
	return dispatch => {
    	return axios.get( API_URL + '/users/' + id ).then(response => dispatch(receiveUser(response.data)));
  	};
}

export function createUser(data) {
	return dispatch => {
    	return axios.post( API_URL + '/users', data ).then(response => dispatch(receiveUser(response.data)));
  	};
}

export function updateUser(id, data) {
  return dispatch => {
      return axios.put( API_URL + '/users/' + id, data ).then(response => dispatch(receiveUser(response.data)));
    };
}

export function deleteUser(id) {
	return dispatch => {
    	return axios.delete( API_URL + '/users/' + id ).then(response => dispatch(receiveUser(response.data)));
  	};
}
