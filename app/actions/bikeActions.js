import * as types from './actionTypes';
import axios from 'axios';
import { API_URL } from '../constants/Globals';

export function receiveBike(data) {
  	return { type: types.RECEIVE_BIKE, bike: data };
}

export function fetchBike(id) {
	return dispatch => {
    	return axios.get( API_URL + '/bikes/' + id ).then(response => dispatch(receiveBike(response.data)));
  	};
}

export function createBike(data) {
	return dispatch => {
    	return axios.post( API_URL + '/bikes', data ).then(response => dispatch(receiveBike(response.data)));
  	};
}

export function updateBike(id, data) {
  return dispatch => {
      return axios.put( API_URL + '/bikes/' + id, data ).then(response => dispatch(receiveBike(response.data)));
    };
}

export function deleteBike(id) {
	return dispatch => {
    	return axios.delete( API_URL + '/bikes/' + id ).then(response => dispatch(receiveBike(response.data)));
  	};
}
