import * as types from './actionTypes';
import axios from 'axios';
import { API_URL } from '../constants/Globals';

export function receiveUsers(data) {
	return { type: types.RECEIVE_USERS, users: data };
}

export function fetchUsers() {
	return dispatch => {
    	return axios.get( API_URL + '/users' ).then(response => dispatch(receiveUsers(response.data)));
  	};
}
