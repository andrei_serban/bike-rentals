import * as types from './actionTypes';
import axios from 'axios';
import { API_URL } from '../constants/Globals';

export function receiveBikes(data) {
	return { type: types.RECEIVE_BIKES, bikes: data };
}

export function fetchBikes(params) {
	let query = '';

	for ( let i in params ) {
		query = query + ( query === '' ? '?' : '&' ) + i + '=' + params[i];
	}

	console.log('query', API_URL + '/bikes' + query);
	
	return dispatch => {
    	return axios.get( API_URL + '/bikes' + query ).then(response => dispatch(receiveBikes(response.data)));
  	};
}
