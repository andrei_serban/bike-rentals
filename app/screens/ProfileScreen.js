import React from 'react';
import { AsyncStorage } from 'react-native';
import { Container, Text, Button } from 'native-base';
import { PaddedArea } from '../components';
import { USER_ID_KEY } from '../constants/Globals';

export default class ProfileScreen extends React.Component {
  static navigationOptions = {
    title: 'Profile',
  };

  onLogout = () => {
  	const { navigation } = this.props;

  	AsyncStorage.removeItem(USER_ID_KEY).then((r) => {
    	navigation.navigate( 'Login' );
  	});
  }

  render() {
    return (
      	<Container>
	        <PaddedArea>
	          <Button block onPress={ this.onLogout }>
	            <Text>Logout</Text>
	          </Button>
	        </PaddedArea>
      	</Container>
    );
  }
}
