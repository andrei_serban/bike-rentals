import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ScrollView, TouchableOpacity, AsyncStorage, ActivityIndicator } from 'react-native';
import { Container, Text, Form, Item, Label, Input, Button } from 'native-base';
import { VerticallyCentered, PaddedArea } from '../../components';
import { API_URL, USER_ID_KEY, USER_ROLES } from '../../constants/Globals';

class LoginScreen extends React.Component {
  static navigationOptions = {
    title: 'Login',
  };

  constructor() {
  	super();

  	this.state = {
  		email: '',
  		password: '',
      loading: false
  	};
  }

  componentDidMount() {
    const { navigation } = this.props;

    AsyncStorage.getItem(USER_ID_KEY).then((id) => {
      if ( id ) {
        this.setState({ loading: true });
        axios.get( API_URL + '/users/' + id ).then((response) => {
          this.setState({ loading: false });
          navigation.navigate( response.data.role === USER_ROLES[0] ? 'Bikes' : 'BikesList' );
        });
      }
    });
  }

  onSubmit = () => {
    const { email, password } = this.state;
    const { navigation } = this.props;

    this.setState({ loading: true });
    console.log('full patg', API_URL + '/auth/login');
    axios.post( API_URL + '/auth/login', { email: email, password: password } ).then((response) => {
      AsyncStorage.setItem(USER_ID_KEY, response.data.id.toString()).then((r) => {
        this.setState({ loading: false });
        navigation.navigate( response.data.role === USER_ROLES[0] ? 'Bikes' : 'BikesList' );
      });
    }).catch((e) => {
      console.log(e);
      this.setState({ loading: false });
      alert('Login failed');
    });
  }

  render() {
  	const { email, password, loading } = this.state;

    return (
      	<Container>
      	  	<VerticallyCentered>
  	          <Form>
  	            <Item floatingLabel>
  	              <Label>Email</Label>
  	              <Input value={ email } keyboardType="email-address" autoCapitalize="none" onChangeText={ (email) => this.setState({ email }) } />
  	            </Item>
  	            <Item floatingLabel last>
  	              <Label>Password</Label>
  	              <Input value={ password } onChangeText={ (password) => this.setState({ password }) } secureTextEntry={true} />
  	            </Item>
  	          </Form>
  	          {
                loading 
                ? <PaddedArea xl>
                    <ActivityIndicator size="large" color="green" />
                </PaddedArea>
                : <PaddedArea>
                  <Button block success onPress={ this.onSubmit }>
                    <Text>Login</Text>
                  </Button>
                </PaddedArea>
              }
          	</VerticallyCentered>
      	</Container>
    );
  }
}

function mapStateToProps(state, action) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen);
