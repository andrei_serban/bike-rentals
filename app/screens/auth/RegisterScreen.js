import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ScrollView, TouchableOpacity, ActivityIndicator, AsyncStorage } from 'react-native';
import { Container, Text, Form, Item, Label, Input, Button, Picker, Icon } from 'native-base';
import { VerticallyCentered, PaddedArea } from '../../components';
import * as userActions from '../../actions/userActions';
import { USER_ID_KEY, USER_ROLES } from '../../constants/Globals';

class RegisterScreen extends React.Component {
  static navigationOptions = {
    title: 'Register',
  };

  constructor() {
  	super();

  	this.state = {
  		name: '',
  		email: '',
  		password: '',
  		confirm_password: '',
      role: '',
      loading: false
  	};
  }

  onSubmit = () => {
    const { name, email, password, confirm_password, role } = this.state;
    const { navigation, userActions } = this.props;
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if ( name === '' ) {
      alert('Please enter your name');
    } else if ( email === '' || !re.test(email) ) {
      alert('Please enter a valid email address');
    } else if ( password.length < 6 ) {
      alert('The password minimum length is 6 characters');
    } else if ( password !== confirm_password ) {
      alert('The passwords are not matching');
    } else if ( role === '' ) {
      alert('Please select the role');
    } else {
      this.setState({ loading: true });
      userActions.createUser({ name: name, email: email, password: password, role: role }).then((response) => {
        AsyncStorage.setItem(USER_ID_KEY, response.user.id.toString()).then((r) => {
          this.setState({ loading: false });
          navigation.navigate( role === USER_ROLES[0] ? 'Bikes' : 'BikesList' );
        });
      }).catch((e) => {
        console.log(e);
        this.setState({ loading: false });
        alert('An error occured');
      });
    }
  }

  render() {
  	const { name, email, password, confirm_password, role, loading } = this.state;
    
    return (
      	<Container>
      	  	<VerticallyCentered>
  	          <Form>
  	            <Item floatingLabel>
  	              <Label>Name</Label>
  	              <Input value={ name } onChangeText={ (name) => this.setState({ name }) } />
  	            </Item>
  	            <Item floatingLabel>
  	              <Label>Email</Label>
  	              <Input value={ email } keyboardType="email-address" autoCapitalize="none" onChangeText={ (email) => this.setState({ email }) } />
  	            </Item>
                <Item floatingLabel>
                  <Label>Password</Label>
                  <Input value={ password } onChangeText={ (password) => this.setState({ password }) } secureTextEntry={true} />
                </Item>
                <Item floatingLabel>
                  <Label>Confirm Password</Label>
                  <Input value={ confirm_password } onChangeText={ (confirm_password) => this.setState({ confirm_password }) } secureTextEntry={true} />
                </Item>
                <Item picker>
                 <PaddedArea top>
                    <Picker
                      mode="dropdown"
                      iosHeader="Select role"
                      iosIcon={<Icon name="arrow-down" />}
                      placeholder="Role"
                      selectedValue={role}
                      onValueChange={(role) => { this.setState({ role }); }}
                    >
                      {USER_ROLES.map((role) => {
                        return (
                          <Picker.Item key={ role } label={ role } value={ role } />
                        );
                      })}
                    </Picker>
                  </PaddedArea>
                </Item>
  	          </Form>
              {
                loading 
                ? <PaddedArea xl>
                    <ActivityIndicator size="large" color="green" />
                </PaddedArea>
                : <PaddedArea>
                  <Button block success onPress={ this.onSubmit }>
                    <Text>Register</Text>
                  </Button>
                </PaddedArea>
              }
          	</VerticallyCentered>
      	</Container>
    );
  }
}

function mapStateToProps(state, action) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterScreen);
