import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, ScrollView, ActivityIndicator, Image, TouchableOpacity, Switch } from 'react-native';
import { ImagePicker, Permissions, MapView } from 'expo';
import { Container, Text, Form, Item, Label, Input, Button, Icon, Picker } from 'native-base';
import { PaddedArea, CustomItem } from '../../components';
import * as bikeActions from '../../actions/bikeActions';
import * as bikesActions from '../../actions/bikesActions';
import { API_URL, BIKE_MODELS, BIKE_COLORS, MIN_WEIGHT, MAX_WEIGHT, MIN_RATE, MAX_RATE } from '../../constants/Globals';

class BikesCreateScreen extends React.Component {
  static navigationOptions = {
    title: 'Bike Details'
  };

  constructor() {
    super();

    this.state = {
      edit: false,
      id: 0,
      name: '',
      model: '',
      image: null,
      color: '',
      weight: '',
      rate: '',
      available: true,
      loading: false,
      markerLocation: {},
      mapLocation: {},
      locationReady: false
    }
  }

  componentDidMount() {
    if ( this.props.navigation.state.routeName === 'BikesView' ) {
      const { bike } = this.props;

      this.setState({
        edit: true,
        id: bike.id,
        name: bike.name,
        model: bike.model,
        color: bike.color,
        weight: bike.weight.toString(),
        rate: bike.rate.toString(),
        available: bike.available > 0,
        image: API_URL + '/images/' + bike.uniqid + '.jpg',
        markerLocation: { latitude: bike.latitude, longitude: bike.longitude },
        mapLocation: { latitude: bike.latitude, longitude: bike.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 },
        locationReady: true
      })
    } else {
      navigator.geolocation.getCurrentPosition((position) => {
        const { latitude, longitude } = position.coords;

        this.setState({
          markerLocation: { latitude: latitude, longitude: longitude },
          mapLocation: { latitude: latitude, longitude: longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 },
          locationReady: true
        });
      },
      (error) => {
        console.log('error', error);
      },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 });
    }
  }

  onSelectImage = () => {
    const self = this;

    Permissions.askAsync(Permissions.CAMERA_ROLL).then((r) => {
      const result = ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [1, 1]
      }).then((image) => {
        if (!image.cancelled) {
          this.setState({ image });
        }
      });
    });
  }

  onSubmit = () => {
    const { edit, id, name, model, image, color, weight, rate, markerLocation, available } = this.state;
    const { navigation, bikeActions, bikesActions } = this.props;
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if ( name === '' || name.trim().length > 100 ) {
      alert('Please enter the name');
    } else if ( image === null ) {
      alert('Please select an image');
    } else if ( model === '' ) {
      alert('Please select the model');
    } else if ( color === '' ) {
      alert('Please select the color');
    } else if ( weight === '' || isNaN(weight) || parseFloat(weight) < MIN_WEIGHT || parseFloat(weight) > MAX_WEIGHT ) {
      alert('Please enter the weight');
    } else if ( rate === '' || isNaN(rate) || parseFloat(rate) < MIN_RATE || parseFloat(rate) > MAX_RATE ) {
      alert('Please enter the rate');
    } else {
      this.setState({ loading: true });
      
      if ( !edit ) {
        bikeActions.createBike({
          name: name,
          model: model, 
          color: color, 
          weight: weight,
          rate: rate,
          latitude: markerLocation.latitude,
          longitude: markerLocation.longitude,
          available: available
        }).then((r) => {
          const bodyFormData = new FormData();
          bodyFormData.append('file', { uri: image.uri, type: 'multipart/form-data', name: 'image.jpg' });

          axios({
            method: 'post',
            url: API_URL + '/bikes/' + r.bike.uniqid + '/photo',
            data: bodyFormData,
            config: { 
              headers: {
                'Content-Type': 'multipart/form-data', 
                'Accept':'application/json'
              } 
            }
          }).then((response) => {
            this.setState({ loading: false });
            bikesActions.fetchBikes();
            navigation.navigate('BikesList');
          }).catch((response) => {
            console.log(response);
            alert('There was an error');
          });
        }).catch((e) => {
          console.log(e);
          this.setState({ loading: false });
          alert('An error occured');
        });
      } else {
        bikeActions.updateBike(id, {
          name: name,
          model: model, 
          color: color, 
          weight: weight,
          rate: rate,
          latitude: markerLocation.latitude,
          longitude: markerLocation.longitude,
          available: available
        }).then((r) => {
          if ( typeof image === 'string' ) {
            this.setState({ loading: false });
            bikesActions.fetchBikes();
            navigation.navigate('BikesList');
          } else {
            const bodyFormData = new FormData();
            bodyFormData.append('file', { uri: image.uri, type: 'multipart/form-data', name: 'image.jpg' });

            axios({
              method: 'post',
              url: API_URL + '/bikes/' + r.bike.uniqid + '/photo',
              data: bodyFormData,
              config: { 
                headers: {
                  'Content-Type': 'multipart/form-data', 
                  'Accept':'application/json'
                } 
              }
            }).then((response) => {
              this.setState({ loading: false });
              bikesActions.fetchBikes();
              navigation.navigate('BikesList');
            }).catch((response) => {
              console.log(response);
              alert('There was an error');
            });
          }
        }).catch((e) => {
          console.log(e);
          this.setState({ loading: false });
          alert('An error occured');
        });
      }
    }
  }

  onDelete = () => {
    const { bike } = this.props;
    const { navigation, bikeActions, bikesActions } = this.props;

    bikeActions.deleteBike(bike.id).then(() => {
      bikesActions.fetchBikes();
      navigation.navigate('BikesList');
    }).catch((e) => {
      console.log(e);
      alert('An error occured');
    });
  }

  render() {
    const { edit, name, model, image, color, weight, rate, available, markerLocation, mapLocation, locationReady, loading } = this.state;

    return (
      <Container>
        <ScrollView keyboardDismissMode="on-drag">
          <Form>
            <Item floatingLabel>
              <Label>Name</Label>
              <Input value={ name } onChangeText={ (name) => this.setState({ name }) } />
            </Item>
            <CustomItem label="Photo">
              {
                image 
                ? (<TouchableOpacity onPress={() => { this.onSelectImage(); }}>
                    <Image source={ typeof image === 'string' ? { uri: image } : image } style={{ width: 120, height: 120 }} />
                </TouchableOpacity>)
                : (<Button block onPress={() => { this.onSelectImage(); }}>
                  <Text>Select</Text>
                </Button>)
              }
            </CustomItem>
            <Item picker>
              <PaddedArea top>
                <Picker
                  mode="dropdown"
                  iosHeader="Select model"
                  iosIcon={<Icon name="arrow-down" />}
                  placeholder="Model"
                  selectedValue={model}
                  onValueChange={(model) => { this.setState({ model }); }}
                >
                  {BIKE_MODELS.map((model) => {
                    return (
                      <Picker.Item key={ model } label={ model } value={ model } />
                    );
                  })}
                </Picker>
              </PaddedArea>
            </Item>
            <Item picker>
              <PaddedArea top>
                <Picker
                  mode="dropdown"
                  iosHeader="Select color"
                  iosIcon={<Icon name="arrow-down" />}
                  placeholder="Color"
                  selectedValue={color}
                  onValueChange={(color) => { this.setState({ color }); }}
                >
                  {BIKE_COLORS.map((color) => {
                    return (
                      <Picker.Item key={ color } label={ color } value={ color } />
                    );
                  })}
                </Picker>
              </PaddedArea>
            </Item>
            <Item floatingLabel>
              <Label>Weight (lbs)</Label>
              <Input value={ weight } onChangeText={ (weight) => this.setState({ weight }) } keyboardType="decimal-pad" />
            </Item>
            <Item floatingLabel>
              <Label>Rate ($)</Label>
              <Input value={ rate } onChangeText={ (rate) => this.setState({ rate }) } keyboardType="decimal-pad" />
            </Item>
            <CustomItem label="Location">
              {
                locationReady ? (<MapView style={{ flex: 1, width: '100%', height: 200 }}
                                          onPress={ (el) => { this.setState({ markerLocation: el.nativeEvent.coordinate }); } }
                                          initialRegion={ mapLocation }>
                                  <MapView.Marker coordinate={ markerLocation }>
                                   <View style={{backgroundColor: '#fff', padding: 5, borderWidth: 1}}>
                                     <Icon name='bicycle' />
                                   </View>
                                  </MapView.Marker>
                                </MapView>)
                : null
              }
            </CustomItem>
            <CustomItem label="Available">
              <Switch value={ available } onValueChange={ (available) => this.setState({ available }) }  />
            </CustomItem>
          </Form>
          {
            loading 
            ? <PaddedArea xl>
              <ActivityIndicator size="large" color="green" />
            </PaddedArea>
            : <PaddedArea>
              <Button block success onPress={ this.onSubmit }>
                <Text>Submit</Text>
              </Button>
            </PaddedArea>
          }

          {
          edit
          ? (<PaddedArea>
              <Button block danger onPress={ this.onDelete }>
                <Text>Delete</Text>
              </Button>
            </PaddedArea>)
          : null
        }
        </ScrollView>
      </Container>
    );
  }
}

function mapStateToProps(state, action) {
  return {
    bike: state.bike
  };
}

function mapDispatchToProps(dispatch) {
  return {
    bikeActions: bindActionCreators(bikeActions, dispatch),
    bikesActions: bindActionCreators(bikesActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BikesCreateScreen);
