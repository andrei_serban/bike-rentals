import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ScrollView, TouchableOpacity, View, Button } from 'react-native';
import { Container, List, ListItem, Text, Left, Right, Icon, Content } from 'native-base';
import { HeaderSide } from '../../components';
import * as bikeActions from '../../actions/bikeActions';
import * as bikesActions from '../../actions/bikesActions';

class BikesListScreen extends React.Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    return {
      title: 'Bikes',
      headerRight: (
        <HeaderSide onPress={() => navigation.navigate('BikesCreate')} name="ios-add"></HeaderSide>
      )
    }
  };

  componentDidMount() {
    this.props.bikesActions.fetchBikes();
  }

  onRead(id) {
    const { navigation, bikeActions, bikesActions } = this.props;
    
    bikeActions.fetchBike(id).then(() => {
      navigation.navigate('BikesView');
    });
  }

  render() {
    const { bikes } = this.props;

    return (
      <Container>
        <ScrollView>
          <List>
            {bikes.map((bike) => {
              return (
                <ListItem key={bike.id} onPress={ () => { this.onRead(bike.id) } }>
                  <Left>
                    <Text>{ bike.name } ({ bike.model })</Text>
                  </Left>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </ListItem>
              );
            })}
          </List>
        </ScrollView>
      </Container>
    );
  }
}

function mapStateToProps(state, action) {
  return {
    bikes: state.bikes
  };
}

function mapDispatchToProps(dispatch) {
  return {
    bikeActions: bindActionCreators(bikeActions, dispatch),
    bikesActions: bindActionCreators(bikesActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BikesListScreen);
