import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ScrollView, TouchableOpacity, View, } from 'react-native';
import { Container, List, ListItem, Text, Left, Right, Icon } from 'native-base';
import { HeaderSide } from '../../components';
import * as userActions from '../../actions/userActions';
import * as usersActions from '../../actions/usersActions';

class UsersListScreen extends React.Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    return {
      title: 'Users',
      headerRight: (
        <HeaderSide onPress={() => navigation.navigate('UsersCreate')} name="ios-add"></HeaderSide>
      )
    }
  };

  componentDidMount() {
    this.props.usersActions.fetchUsers();
  }

  onRead(id) {
    const { navigation, userActions } = this.props;
    
    userActions.fetchUser(id).then(() => {
      navigation.navigate('UsersView');
    });
  }

  render() {
    const self = this;
    const { users } = this.props;

    return (
      <Container>
        <ScrollView>
          <List>
            {users.map((user) => {
              return (
                <ListItem key={user.id} onPress={ () => { self.onRead(user.id) } }>
                  <Left>
                    <Text>{ user.name } ({ user.role })</Text>
                  </Left>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </ListItem>
              );
            })}
          </List>
        </ScrollView>
      </Container>
    );
  }
}

function mapStateToProps(state, action) {
  return {
    users: state.users
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    usersActions: bindActionCreators(usersActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersListScreen);
