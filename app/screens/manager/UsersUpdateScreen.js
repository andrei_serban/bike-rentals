import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, ActivityIndicator, AsyncStorage } from 'react-native';
import { Container, Text, Form, Item, Label, Input, Button, Picker, Icon } from 'native-base';
import { PaddedArea } from '../../components';
import * as userActions from '../../actions/userActions';
import * as usersActions from '../../actions/usersActions';
import { USER_ROLES, USER_ID_KEY } from '../../constants/Globals';

class UsersUpdateScreen extends React.Component {
  static navigationOptions = {
    title: 'User Details'
  };

  constructor() {
  	super();

  	this.state = {
      edit: false,
      currentUserId: 0,
      id: 0,
  		name: '',
  		email: '',
  		password: '',
  		confirm_password: '',
  		role: '',
  		loading: false
  	}
  }

  componentDidMount() {
    AsyncStorage.getItem(USER_ID_KEY).then((id) => {
      if ( id ) {
        this.setState({ currentUserId: id });
      }
    });

    if ( this.props.navigation.state.routeName === 'UsersView' ) {
      const user = this.props.user;

      this.setState({
        edit: true,
        id: user.id,
        name: user.name,
        email: user.email,
        role: user.role
      })
    }
  }

  onSubmit = () => {
  	const { edit, id, name, email, password, confirm_password, role } = this.state;
  	const { navigation, userActions, usersActions } = this.props;
  	const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    
    if ( name === '' ) {
      alert('Please enter your name');
    } else if ( email === '' || !re.test(email) ) {
      alert('Please enter a valid email address');
    } else if ( password.length < 6 ) {
      alert('The password minimum length is 6 characters');
    } else if ( password !== confirm_password ) {
      alert('The passwords are not matching');
    } else if ( role === '' ) {
      alert('Please select the role');
    } else {
      this.setState({ loading: true });
      if ( !edit ) {
        userActions.createUser({ name: name, email: email, password: password, role: role }).then((r) => {
          this.setState({ loading: false });
          usersActions.fetchUsers();
          navigation.navigate('UsersList');
        }).catch((e) => {
          console.log(e);
          this.setState({ loading: false });
          alert('An error occured');
        });
      } else {
        userActions.updateUser(id, { name: name, email: email, password: password, role: role }).then((r) => {
          this.setState({ loading: false });
          usersActions.fetchUsers();
          navigation.navigate('UsersList');
        }).catch((e) => {
          console.log(e);
          this.setState({ loading: false });
          alert('An error occured');
        });
      }
    }
  }

  onDelete = () => {
    const { user } = this.props;
    const { navigation, userActions, usersActions } = this.props;

    userActions.deleteUser(user.id).then(() => {
      usersActions.fetchUsers();
      navigation.navigate('UsersList');
    }).catch((e) => {
      console.log(e);
      alert('An error occured');
    });
  }

  render() {
  	const { currentUserId, id, edit, name, email, password, confirm_password, role, loading } = this.state;

    return (
      <Container>
        <Form>
          <Item floatingLabel>
            <Label>Name</Label>
            <Input value={ name } onChangeText={ (name) => this.setState({ name }) } />
          </Item>
          <Item floatingLabel>
            <Label>Email</Label>
            <Input value={ email } textContentType="emailAddress" keyboardType="email-address" autoCapitalize="none" onChangeText={ (email) => this.setState({ email }) } />
          </Item>
          <Item floatingLabel>
            <Label>Password</Label>
            <Input value={ password } onChangeText={ (password) => this.setState({ password }) } secureTextEntry={true} />
          </Item>
          <Item floatingLabel>
            <Label>Confirm Password</Label>
            <Input value={ confirm_password } onChangeText={ (confirm_password) => this.setState({ confirm_password }) } secureTextEntry={true} />
          </Item>
          <Item picker>
    	     <PaddedArea top>
            	<Picker
	              mode="dropdown"
	              iosHeader="Select role"
	              iosIcon={<Icon name="arrow-down" />}
	              placeholder="Role"
	              selectedValue={role}
	              onValueChange={(role) => { this.setState({ role }); }}
	            >
                {USER_ROLES.map((role) => {
                  return (
                    <Picker.Item key={ role } label={ role } value={ role } />
                  );
                })}
              </Picker>
            </PaddedArea>
          </Item>
        </Form>
      	{
      		loading 
      		? <PaddedArea xl>
        			<ActivityIndicator size="large" color="green" />
    			</PaddedArea>
        	: <PaddedArea>
        		<Button block success onPress={ this.onSubmit }>
  		  		  <Text>Submit</Text>
        		</Button>
      		</PaddedArea>
      	}

        {
          edit && ( currentUserId > id || currentUserId < id )
          ? (<PaddedArea>
              <Button block danger onPress={ this.onDelete }>
                <Text>Delete</Text>
              </Button>
            </PaddedArea>)
          : null
        }
  	</Container>
    );
  }
}

function mapStateToProps(state, action) {
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    usersActions: bindActionCreators(usersActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersUpdateScreen);
