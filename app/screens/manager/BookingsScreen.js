import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ScrollView, TouchableOpacity, View, Image } from 'react-native';
import { Container, List, ListItem, Text, Left, Right, Icon, Tabs, Tab, Button } from 'native-base';
import { HeaderSide } from '../../components';
import * as usersActions from '../../actions/usersActions';
import * as bikesActions from '../../actions/bikesActions';
import * as bookingsActions from '../../actions/bookingsActions';

class BookingsScreen extends React.Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    return {
      title: 'Bookings'
    }
  };

  constructor() {
    super();

    this.state = {
      selectedUser: 0,
      selectedBike: 0
    };
  }

  componentDidMount() {
    this.props.usersActions.fetchUsers();
    this.props.bikesActions.fetchBikes();
  }

  onExpand = (selectedBike, selectedUser) => {
    this.setState({
      selectedBike: selectedBike,
      selectedUser: selectedUser
    });

    this.props.bookingsActions.fetchBookings( selectedBike > 0 ? { bike_id: selectedBike } : { user_id: selectedUser } );
  }

  render() {
    const { selectedUser, selectedBike } = this.state;
    const { users, bikes, bookings } = this.props;

    return (
      <Container>
        <Tabs>
          
          <Tab heading="By User">
            <ScrollView>
              <List>
                {users.map((user) => {
                  return (
                    <View key={user.id}>
                      <ListItem onPress={ () => { this.onExpand(0, selectedUser === user.id ? 0 : user.id) } }>
                        <Left>
                          <Text>{ user.name }</Text>
                        </Left>
                        <Right>
                          <Icon name={ selectedUser === user.id ? 'arrow-up' : 'arrow-down' } />
                        </Right>
                      </ListItem>
                      {
                        selectedUser === user.id 
                        ? (<View>
                          {
                            bookings.length < 1
                            ? (<ListItem style={{ backgroundColor: '#eee', paddingLeft: 15 }}>
                                <Text>No bookings yet...</Text>
                            </ListItem>)
                            : (<View>
                              {bookings.map((booking) => {
                                return (
                                  <View key={booking.id}>
                                    <ListItem style={{ backgroundColor: '#eee', paddingLeft: 15 }}>
                                      <Left>
                                        <Text>{ booking.bike_name } on { moment(booking.date).format('MM/DD/YYYY') }</Text>
                                      </Left>
                                    </ListItem>
                                  </View>
                                );
                              })}
                            </View>)
                          }
                        </View>)
                        : null
                      }
                    </View>
                  );
                })}
              </List>
            </ScrollView>
          </Tab>
          
          <Tab heading="By Bike">
            <ScrollView>
              <List>
                {bikes.map((bike) => {
                  return (
                    <View key={bike.id}>
                      <ListItem onPress={ () => { this.onExpand(selectedBike === bike.id ? 0 : bike.id, 0) } }>
                        <Left>
                          <Text>{ bike.name }</Text>
                        </Left>
                        <Right>
                          <Icon name={ selectedBike === bike.id ? 'arrow-up' : 'arrow-down' } />
                        </Right>
                      </ListItem>
                      {
                        selectedBike === bike.id
                        ? (<View>
                          {
                            bookings.length < 1
                            ? (<ListItem style={{ backgroundColor: '#eee', paddingLeft: 15 }}>
                                <Text>No bookings yet...</Text>
                            </ListItem>)
                            : (<View>
                              {bookings.map((booking) => {
                                return (
                                  <View key={booking.id}>
                                    <ListItem style={{ backgroundColor: '#eee', paddingLeft: 15 }}>
                                      <Left>
                                        <Text>{ booking.user_name } on { moment(booking.date).format('MM/DD/YYYY') }</Text>
                                      </Left>
                                    </ListItem>
                                  </View>
                                );
                              })}
                            </View>)
                          }
                        </View>)
                        : null
                      }
                    </View>
                  );
                })}
              </List>
            </ScrollView>
          </Tab>
        
        </Tabs>
      </Container>
    );
  }
}

function mapStateToProps(state, action) {
  return {
    users: state.users,
    bikes: state.bikes,
    bookings: state.bookings
  };
}

function mapDispatchToProps(dispatch) {
  return {
    usersActions: bindActionCreators(usersActions, dispatch),
    bikesActions: bindActionCreators(bikesActions, dispatch),
    bookingsActions: bindActionCreators(bookingsActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookingsScreen);
