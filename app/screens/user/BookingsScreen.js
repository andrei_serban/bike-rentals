import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ScrollView, TouchableOpacity, View, AsyncStorage, StyleSheet } from 'react-native';
import { Container, List, ListItem, Text, Left, Right, Icon, Button } from 'native-base';
import { HeaderSide } from '../../components';
import * as bookingActions from '../../actions/bookingActions';
import * as bookingsActions from '../../actions/bookingsActions';
import { USER_ID_KEY } from '../../constants/Globals';

class BookingsScreen extends React.Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    return {
      title: 'Bookings'
    }
  };

  constructor() {
    super();

    this.state = {
      userId: 0,
      selectedBooking: 0
    };
  }

  componentDidMount() {
    AsyncStorage.getItem(USER_ID_KEY).then((userId) => {
      if ( userId ) {
        this.props.bookingsActions.fetchBookings({ user_id: userId });
        this.setState({ userId });
      }
    });
  }

  onSetRating(id, rating) {
    const { userId } = this.state;
    const { bookingActions, bookingsActions } = this.props;
    
    bookingActions.updateBooking(id, { rating }).then((r) => {
      bookingsActions.fetchBookings({ user_id: userId });
    });
  }

  onCancel(id) {
    const { userId } = this.state;
    const { bookingActions, bookingsActions } = this.props;

    bookingActions.deleteBooking(id).then((r) => {
      bookingsActions.fetchBookings({ user_id: userId });
    });
  }

  render() {
    const { selectedBooking } = this.state;
    const { bookings } = this.props;

    return (
      <Container>
        <ScrollView>
          <List>
            {bookings.map((booking) => {
              return (
                <View key={booking.id}>
                  <ListItem onPress={ () => { this.setState({ selectedBooking: selectedBooking === booking.id ? 0 : booking.id }) } }>
                    <Left>
                      <Text>{ booking.bike_name } on { moment(booking.date).format('MM/DD/YYYY') }</Text>
                    </Left>
                    <Right>
                      <Icon name={ selectedBooking === booking.id ? 'arrow-up' : 'arrow-down' } />
                    </Right>
                  </ListItem>
                  {
                    selectedBooking === booking.id
                    ? (<ListItem>
                        <Left>
                          <Button danger block onPress={ () => { this.onCancel(booking.id); } }>
                            <Text>Cancel</Text>
                          </Button>
                        </Left>
                        <Right style={ styles.ratingHolder }>
                          <TouchableOpacity style={ styles.ratingStar } onPress={ () => { this.onSetRating(booking.id, 1) } }>
                            <Icon name={ booking.rating >= 1 ? 'star' : 'star-outline' } style={ styles.ratingStarIcon }></Icon>
                          </TouchableOpacity>
                          <TouchableOpacity style={ styles.ratingStar } onPress={ () => { this.onSetRating(booking.id, 2) } }>
                            <Icon name={ booking.rating >= 2 ? 'star' : 'star-outline' } style={ styles.ratingStarIcon }></Icon>
                          </TouchableOpacity>
                          <TouchableOpacity style={ styles.ratingStar } onPress={ () => { this.onSetRating(booking.id, 3) } }>
                            <Icon name={ booking.rating >= 3 ? 'star' : 'star-outline' } style={ styles.ratingStarIcon }></Icon>
                          </TouchableOpacity>
                          <TouchableOpacity style={ styles.ratingStar } onPress={ () => { this.onSetRating(booking.id, 4) } }>
                            <Icon name={ booking.rating >= 4 ? 'star' : 'star-outline' } style={ styles.ratingStarIcon }></Icon>
                          </TouchableOpacity>
                          <TouchableOpacity style={ styles.ratingStar } onPress={ () => { this.onSetRating(booking.id, 5) } }>
                            <Icon name={ booking.rating >= 5 ? 'star' : 'star-outline' } style={ styles.ratingStarIcon }></Icon>
                          </TouchableOpacity>
                        </Right>
                      </ListItem>)
                    : null
                  }
                </View>
              );
            })}
          </List>
        </ScrollView>
      </Container>
    );
  }
}

function mapStateToProps(state, action) {
  return {
    bookings: state.bookings
  };
}

function mapDispatchToProps(dispatch) {
  return {
    bookingActions: bindActionCreators(bookingActions, dispatch),
    bookingsActions: bindActionCreators(bookingsActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookingsScreen);

const styles = StyleSheet.create({
  ratingHolder: {
    flex: 1, 
    flexDirection: 'row', 
    alignItems: 'flex-end', 
    justifyContent: 'flex-end'
  },
  ratingStar: {
    paddingLeft: 5,
    paddingRight: 5
  },
  ratingStarIcon: {
    fontSize: 30,
    color: 'orange'
  }
});
