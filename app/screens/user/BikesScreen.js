import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { Slider, View, Image, ActivityIndicator, AsyncStorage } from 'react-native';
import { Container, Content, Text, List, ListItem, Left, Right, Icon, Button, Picker } from 'native-base';
import { MapView } from 'expo';
import { PaddedArea } from '../../components';
import * as bikesActions from '../../actions/bikesActions';
import * as bookingActions from '../../actions/bookingActions';
import * as bookingsActions from '../../actions/bookingsActions';
import { API_URL, USER_ID_KEY, BIKE_MODELS, BIKE_COLORS, MIN_WEIGHT, MAX_WEIGHT, MIN_RATE, MAX_RATE } from '../../constants/Globals';

class BikesScreen extends React.Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    return {
      title: 'Bikes'
    }
  };

  constructor(props) {
    super(props);
    
	this.state = { 
		userId: 0,
    	datepickerVisible: false,
    	filtersVisible: false,
    	date: moment(),
    	model: '',
    	color: '',
    	minWeight: MIN_WEIGHT,
    	maxWeight: MAX_WEIGHT,
    	minRate: MIN_RATE,
    	maxRate: MAX_RATE,
    	selectedBike: 0,
      mapLocation: {},
    };
  }

  componentDidMount() {
    this.props.bikesActions.fetchBikes({ available: 1 });

    AsyncStorage.getItem(USER_ID_KEY).then((id) => {
      if ( id ) {
      	this.setState({ userId: id });
      }
    });

    navigator.geolocation.getCurrentPosition((position) => {
      const latitude = position.coords.latitude;
      const longitude = position.coords.longitude;

      this.setState({
        mapLocation: { latitude: latitude, longitude: longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 },
        locationReady: true
      });
    },
    (error) => {
      console.log('error', error);
    },
    { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 });
  }
  
  handleDatePicked = (newDate) => {
    this.setState({
    	date: moment(newDate),
    	datepickerVisible: false,
    	filtersVisible: false
    });
  }

  onBook(bikeId) {
  	const { userId, date } = this.state;
  	const { navigation, bikesActions, bookingActions, bookingsActions } = this.props;

  	this.setState({ loading: bikeId });
  	bookingActions.createBooking({ user_id: userId, bike_id: bikeId, date: date.format('YYYY-MM-DD') }).then((r) => {
      this.setState({ loading: 0 });
      bookingsActions.fetchBookings({ user_id: userId });
      navigation.navigate('Bookings');
    }).catch((e) => {
      console.log(e);
      this.setState({ loading: 0 });
      alert('An error occured');
    });
  }

  onFilter = () => {
  	const { model, color, minWeight, maxWeight, minRate, maxRate } = this.state;
  	
  	this.setState({ filtersVisible: false });
  	this.props.bikesActions.fetchBikes({
  		available: 1,
  		model: model, 
  		color: color, 
  		min_weight: minWeight, 
  		max_weight: maxWeight, 
  		min_rate: minRate, 
  		max_rate: maxRate 
  	});
  }

  onSelect = (bike) => {
    const { selectedBike } = this.state;
    
    this.setState({ filtersVisible: false, selectedBike: selectedBike === bike.id ? 0 : bike.id });
  }
  
  render() {
  	const self = this;
  	const { filtersVisible, 
            datepickerVisible, 
            date, 
            model, 
            color, 
            minWeight, 
            maxWeight, 
            minRate, 
            maxRate, 
            selectedBike, 
            loading,
            mapLocation,
            locationReady
          } = this.state;
  	const { bikes } = this.props;
    
    return (
      <Container>
        <Content>
        	<List>
        		<ListItem>
        			<Left>
        				<Text>Bikes available on: { date.format('MM/DD/Y') }</Text>
          				<DateTimePicker
          					isVisible={datepickerVisible}
          					date={date.toDate()}
          					onConfirm={this.handleDatePicked}
          					onCancel={ () => { this.setState({ datepickerVisible: false }) } }
        				/>
    					</Left>
    					<Right>
    						<Button block onPress={ () => { this.setState({ datepickerVisible: true }) } }>
      		  		  			<Icon name="arrow-down" />
            				</Button>
    					</Right>
    					<Right style={{ paddingLeft: 5 }}>
    						<Button block onPress={ () => { this.setState({ filtersVisible: !filtersVisible }) } }>
      		  		  			<Icon name="menu" />
        					</Button>
    					</Right>
        		</ListItem>
      		</List>
    		{
    			!filtersVisible
				? null
				: (<List style={{ backgroundColor: '#eee' }}>
	    			  <ListItem>
						          <Picker
								          style={{ marginLeft: -15 }}
		                  		mode="dropdown"
			                  	iosHeader="Select model"
			                  	iosIcon={<Icon name="arrow-down" />}
			                  	placeholder="Model"
			                  	selectedValue={model}
			                  	onValueChange={(model) => { this.setState({ model }); }}
			                >
			                  	<Picker.Item label="" value="" />
			                  	{BIKE_MODELS.map((model) => {
			                    	return (
			                      		<Picker.Item key={ model } label={ model } value={ model } />
			                    	);
			                  	})}
			                </Picker>
		    			</ListItem>
		    			<ListItem>
						          <Picker
								          style={{ marginLeft: -15 }}
			                  	mode="dropdown"
			                  	iosHeader="Select color"
			                  	iosIcon={<Icon name="arrow-down" />}
			                  	placeholder="Model"
			                  	selectedValue={color}
			                  	onValueChange={(color) => { this.setState({ color }); }}
			                >
			                	<Picker.Item label="" value="" />
			                  	{BIKE_COLORS.map((color) => {
				                    return (
				                      <Picker.Item key={ color } label={ color } value={ color } />
				                    );
			                  	})}
			                </Picker>
		    			</ListItem>
		    			<ListItem>
		    				<Left>
		    					<Text>Min. weight: { minWeight } lbs.</Text>
  							</Left>
		    				<Right>
		    					<Slider minimumValue={ MIN_WEIGHT } 
		    							maximumValue={ MAX_WEIGHT } 
		    							value={ minWeight } 
		    							step={ 1 }
		    							style={{ width: 180 }} 
		    							onValueChange={ (minWeight) => { this.setState({ minWeight }); } }
    							/>
  							</Right>
		    			</ListItem>
		    			<ListItem>
		    				<Left>
		    					<Text>Max. weight: { maxWeight } lbs.</Text>
  							</Left>
		    				<Right>
		    					<Slider minimumValue={ MIN_WEIGHT } 
		    							maximumValue={ MAX_WEIGHT } 
		    							value={ maxWeight } 
		    							step={ 1 }
		    							style={{ width: 180 }} 
		    							onValueChange={ (maxWeight) => { this.setState({ maxWeight }); } }
    							/>
  							</Right>
		    			</ListItem>
		    			<ListItem>
		    				<Left>
		    					<Text>Min. rate: ${ minRate }</Text>
  							</Left>
		    				<Right>
  		    					<Slider minimumValue={ MIN_RATE } 
  		    							maximumValue={ MAX_RATE } 
  		    							value={ minRate } 
  		    							step={ 1 }
  		    							style={{ width: 180 }} 
  		    							onValueChange={ (minRate) => { this.setState({ minRate }); } }
      							/>
                </Right>
		    			</ListItem>
		    			<ListItem>
		    				<Left>
		    					<Text>Max. rate: ${ maxRate }</Text>
                </Left>
		    				<Right>
		    					<Slider minimumValue={ MIN_RATE } 
		    							maximumValue={ MAX_RATE } 
	    								value={ maxRate } 
	    								step={ 1 }
	    								style={{ width: 180 }} 
	    								onValueChange={ (maxRate) => { this.setState({ maxRate }); } }
	    						/>
							</Right>
		    			</ListItem>
  						<PaddedArea>
  		    				<Button block onPress={ this.onFilter }>
  		    					<Text>Filter</Text>
  		    				</Button>
  						</PaddedArea>
		    		</List>
				  )
    		}

          {
            locationReady
            ? (<MapView style={{ flex: 1, width: '100%', height: 200 }} initialRegion={ mapLocation }>
                {bikes.map((bike) => {
                  return (
                    <MapView.Marker key={ bike.id } coordinate={ { latitude: bike.latitude, longitude: bike.longitude } } onPress={ () => { this.onSelect(bike) } }>
                     <View style={{backgroundColor: '#fff', padding: 5, borderWidth: 1}}>
                       <Icon name='bicycle' />
                     </View>
                    </MapView.Marker>          
                  );
                })}
              </MapView>)
            : null
          }
    		
            <List>
            	{bikes.map((bike) => {
              		return (
                		<View key={bike.id}>
                			<ListItem onPress={ () => { this.onSelect(bike) } }>
                  				<Left>
                    				<Text>{ bike.name } ({ bike.model })</Text>
                  				</Left>
                  				<Right>
                    				<Icon name={ selectedBike === bike.id ? 'arrow-up' : 'arrow-down' } />
                  				</Right>
                			</ListItem>
                			{
                				selectedBike === bike.id
                				? (<ListItem style={{ backgroundColor: '#eee', paddingLeft: 15 }}>
                  					<Left>
                  						<List>
                  							<Text>
                									Name: { bike.name }
                								</Text>
                								<Text>
                									Model: { bike.model }
                								</Text>
                								<Text>
                									Color: { bike.color }
                								</Text>
                								<Text>
                									Weight: { bike.weight } lbs.
                								</Text>
                								<Text>
                									Rate: ${ bike.rate }
                								</Text>
                      					<PaddedArea top>
  	                    						{
  	                    							loading === bike.id
  	                    							? <ActivityIndicator size="large" color="blue" />
  	                    							: (<Button onPress={ () => { this.onBook(bike.id); } }>
        			          									<Text>Book Now</Text>
        			          								</Button>)
  	                    						}
  	          								 </PaddedArea>
                  						</List>
              	  					</Left>
              	  					<Right>
                    					<Image source={{ uri: API_URL + '/images/' + bike.uniqid + '.jpg' }} style={{ width: 100, height: 100 }} />
                  					</Right>
            					</ListItem>)
                				: null
                			}
                		</View>
              		);
            	})}
          	</List>
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state, action) {
  return {
    bikes: state.bikes
  };
}

function mapDispatchToProps(dispatch) {
  return {
  	bookingActions: bindActionCreators(bookingActions, dispatch),
    bikesActions: bindActionCreators(bikesActions, dispatch),
    bookingsActions: bindActionCreators(bookingsActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BikesScreen);

