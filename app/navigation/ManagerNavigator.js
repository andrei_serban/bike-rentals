import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import BikesListScreen from '../screens/manager/BikesListScreen';
import BikesUpdateScreen from '../screens/manager/BikesUpdateScreen';
import UsersListScreen from '../screens/manager/UsersListScreen';
import UsersUpdateScreen from '../screens/manager/UsersUpdateScreen';
import BookingsScreen from '../screens/manager/BookingsScreen';
import ProfileScreen from '../screens/ProfileScreen';

const BikesStack = createStackNavigator({
  BikesList: BikesListScreen,
  BikesCreate: BikesUpdateScreen,
  BikesView: BikesUpdateScreen,
});

BikesStack.navigationOptions = {
  tabBarLabel: 'Bikes',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-bicycle' : 'md-bicycle'}
    />
  ),
};

const UsersStack = createStackNavigator({
  UsersList: UsersListScreen,
  UsersCreate: UsersUpdateScreen,
  UsersView: UsersUpdateScreen,
});

UsersStack.navigationOptions = {
  tabBarLabel: 'Users',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-person' : 'md-person'}
    />
  ),
};

const BookingsStack = createStackNavigator({
  BookingsList: BookingsScreen,
});

BookingsStack.navigationOptions = {
  tabBarLabel: 'Bookings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-card' : 'md-card'}
    />
  ),
};

const ProfileStack = createStackNavigator({
  Profile: ProfileScreen,
});

ProfileStack.navigationOptions = {
  tabBarLabel: 'Profile',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-log-out' : 'md-log-out'}
    />
  ),
};

export default createBottomTabNavigator({
  BikesStack,
  UsersStack,
  BookingsStack,
  ProfileStack
});
