import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import BikesScreen from '../screens/user/BikesScreen';
import BookingsScreen from '../screens/user/BookingsScreen';
import ProfileScreen from '../screens/ProfileScreen';

const BikesStack = createStackNavigator({
  Bikes: BikesScreen,
});

BikesStack.navigationOptions = {
  tabBarLabel: 'Bikes',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-bicycle' : 'md-bicycle'}
    />
  ),
};

const BookingsStack = createStackNavigator({
  Bookings: BookingsScreen,
});

BookingsStack.navigationOptions = {
  tabBarLabel: 'Bookings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-card' : 'md-card'}
    />
  ),
};

const ProfileStack = createStackNavigator({
  Profile: ProfileScreen,
});

ProfileStack.navigationOptions = {
  tabBarLabel: 'Profile',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-log-out' : 'md-log-out'}
    />
  ),
};

export default createBottomTabNavigator({
  BikesStack,
  BookingsStack,
  ProfileStack,
});
