import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import AuthNavigator from './AuthNavigator';
import ManagerNavigator from './ManagerNavigator';
import UserNavigator from './UserNavigator';

export default createAppContainer(createSwitchNavigator({
  Auth: AuthNavigator,
  Manager: ManagerNavigator,
  User: UserNavigator,
}, {
	initialRouteName: 'Auth'
}));
