import initialState from './initialState';
import { FETCH_BOOKINGS, RECEIVE_BOOKINGS } from '../actions/actionTypes';

export default function bookings(state = initialState.bookings, action) {
  let newState;

  switch (action.type) {
    case FETCH_BOOKINGS:
      return action;

    case RECEIVE_BOOKINGS:
      newState = action.bookings;

      return newState;

    default:
      return state;
  }
}
