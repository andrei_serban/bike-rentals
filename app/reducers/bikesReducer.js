import initialState from './initialState';
import { FETCH_BIKES, RECEIVE_BIKES } from '../actions/actionTypes';

export default function bikes(state = initialState.bikes, action) {
  let newState;

  switch (action.type) {
    case FETCH_BIKES:
      return action;

    case RECEIVE_BIKES:
      newState = action.bikes;

      return newState;

    default:
      return state;
  }
}
