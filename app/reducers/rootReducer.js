import {combineReducers} from 'redux';
import user from './userReducer';
import users from './usersReducer';
import bike from './bikeReducer';
import bikes from './bikesReducer';
import booking from './bookingReducer';
import bookings from './bookingsReducer';

const rootReducer = combineReducers({
	user,
	users,
	bike,
	bikes,
	booking,
	bookings
});

export default rootReducer;
