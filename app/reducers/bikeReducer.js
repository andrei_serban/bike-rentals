import initialState from './initialState';
import { FETCH_BIKE, RECEIVE_BIKE } from '../actions/actionTypes';

export default function bike(state = initialState.bike, action) {
  let newState;

  switch (action.type) {
    case FETCH_BIKE:
      return action;

    case RECEIVE_BIKE:
      newState = action.bike;

      return newState;

    default:
      return state;
  }
}
