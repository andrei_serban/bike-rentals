import initialState from './initialState';
import { FETCH_BOOKING, RECEIVE_BOOKING } from '../actions/actionTypes';

export default function booking(state = initialState.booking, action) {
  let newState;

  switch (action.type) {
    case FETCH_BOOKING:
      return action;

    case RECEIVE_BOOKING:
      newState = action.booking;

      return newState;

    default:
      return state;
  }
}
