export default {
	user: null,
	users: [],

	bike: null,
	bikes: [],

	booking: null,
	bookings: []
};
